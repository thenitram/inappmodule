//
//  StoreData.h
//  TheStore
//
//  Created by Martin on 10/2/17.
//  Copyright © 2017 Martin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

typedef NS_ENUM(NSInteger, PurchaseStatus) {
    SUCCESS,
    FAILED,
    RESTORED,
    PURCHASING,
};

@interface TransactionResponse : NSObject
@property (nonatomic, assign) SKProduct *product;
@property (nonatomic, assign) PurchaseStatus status;
@property (nonatomic, assign) NSString *restoredProductId;
@end
