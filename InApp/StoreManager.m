/*
 * Copyright (C) 2017  John Martin Magalong
 *
 * This file is part of InAppModule.
 * InAppModule free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * InAppModule is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

#import "StoreManager.h"
#import "Constants.h"

@interface StoreManager (){
    UIActivityIndicatorView *activityIndicatorView;
}
@end  
@implementation StoreManager

static StoreManager *_instance = nil;

//NOTE: Don't delete this singleton API so you can import this to other project w/o the ManagerFactory.
+(StoreManager *)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc]init];
    });
    return _instance;
}

#pragma mark- Lifecycle
- (id)init{
    if (!_instance) {
        self = [super init];
        if (self) {
            _instance = self;
            notificationCenter  = [[NSNotificationCenter alloc]init];
        }
    }
    return self;
}

- (void)dealloc{
    productsRequest.delegate = nil;
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

#pragma mark- Private
- (BOOL)canMakePurchases{
    return [SKPaymentQueue canMakePayments];
}

- (void)showSpinner{
    if(!activityIndicatorView){
        activityIndicatorView = [[UIActivityIndicatorView alloc]
                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicatorView.center = [[UIApplication sharedApplication]keyWindow].center;
    }
    
    UIViewController *vc = [[UIApplication sharedApplication] keyWindow].rootViewController;
    while(vc.presentedViewController != nil){
        vc = vc.presentedViewController;
    }
    [vc.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideSpinner{
    [activityIndicatorView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

#pragma mark- Public
- (void)fetchAvailableProducts{
    NSSet *productIdentifiers = [NSSet setWithObjects:
                                 Constants.InApp.Product1, Constants.InApp.Product2, Constants.InApp.Product3,
                                 Constants.InApp.Product4, Constants.InApp.Product5, Constants.InApp.Product6,
                                nil];
    
    if(productsRequest != nil){
        productsRequest.delegate = nil;
    }
    
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}


- (void)restoreProducts{
    //No _products = NO fetched data = NO internet;
    if(products == nil || products.count == 0){
        [self showNoInternetAlert];
        return;
    }
    
    [self showSpinner];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)purchaseProduct:(SKProduct*) product{
    //No _products = NO fetched data = NO internet;
    if(products == nil || products.count == 0){
        [self showNoInternetAlert];
        return;
    }
    
    [self showSpinner];
    if ([self canMakePurchases]) {
        if(product != nil){
            SKPayment *payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Purchases are disabled in your device."
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
        
        [alert addAction:yesButton];
        [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
        yesButton = nil;
        alert = nil;
    }
}

- (NSArray<SKProduct*>*)getProducts{
    if(products == nil || products.count == 0){
        [self showNoInternetAlert];
    }
    return products;
}

- (void)registerObserver:(id)observer selector:(SEL)selector name:name{
    [notificationCenter addObserver:observer selector:selector name:name object:nil];
}

- (void)unregisterObserver:(id)observer name:name{
    [notificationCenter removeObserver:observer name:name object:nil];
}

#pragma mark StoreKit Delegate
-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions 
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        //transaction.payment.productIdentifier
        TransactionResponse *response = [[TransactionResponse alloc]init];
        
        //Get the product from the fetched products.
        for(int x=0; x<products.count; x++){
            SKProduct *temp = [products objectAtIndex:x];
            if([temp.productIdentifier isEqualToString:transaction.payment.productIdentifier]){
                response.product = temp;
                break;
            }
        }

        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                response.status = PURCHASING;
                break;                
            case SKPaymentTransactionStatePurchased:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                response.status = SUCCESS;
                [self hideSpinner];
                break;                
            case SKPaymentTransactionStateRestored:               
                NSLog(@"Restored ");               
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                response.status = RESTORED;
                response.restoredProductId = transaction.payment.productIdentifier;
                [self hideSpinner];
                break;                
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");             
                response.status = FAILED;
                [self hideSpinner];
                break;
            default:
                break;
        }
        [notificationCenter postNotificationName:StoreEventTransactionResult object:response];
        [_delegate transactionResult:response];
    }
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    products = [[NSArray alloc]initWithArray:response.products];
    [_delegate productRequestCallback:[[NSArray alloc]initWithArray:response.products]];
    [notificationCenter postNotificationName:StoreEventFetchProduct object:response];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    if (error.code == 110){
        [self showNoInternetAlert];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    if (error.code == SKErrorPaymentCancelled){
        
    }
    [self hideSpinner];
}

#pragma mark -
- (void)showNoInternetAlert{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Connection Error!"
                                 message:@"Please check you internet connection and go back to Home Screen before trying again."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* button = [UIAlertAction
                             actionWithTitle:@"Dismiss"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [alert dismissViewControllerAnimated:true completion:nil];
                             }];
    
    [alert addAction:button];
    button = nil;
    
    
    UIViewController *vc = [[UIApplication sharedApplication] keyWindow].rootViewController;
    while(vc.presentedViewController != nil){
        vc = vc.presentedViewController;
    }
    [vc presentViewController:alert animated:YES completion:nil];
    alert = nil;
}
@end
