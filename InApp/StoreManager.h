/*
 * Copyright (C) 2017  John Martin Magalong
 *
 * This file is part of InAppModule.
 * InAppModule free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * InAppModule is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "TransactionResponse.h"
#import "Constants.h"

@protocol StoreManagerDelegate <NSObject>
@required
- (void)productRequestCallback:(NSArray<SKProduct*> *)products;
- (void)transactionResult:(TransactionResponse* )response;
@end


@interface StoreManager : NSObject<SKProductsRequestDelegate, SKPaymentTransactionObserver>{
    SKProductsRequest *productsRequest;
    NSNotificationCenter *notificationCenter;
    NSArray<SKProduct*> *products;
}

@property (nonatomic, assign) id<StoreManagerDelegate> delegate;

- (id)init OBJC_DEPRECATED("Do NOT use this init if ManagerFactory class exists. Use ManagerFactory method to get instance of this class.");
- (void)fetchAvailableProducts;
- (void)restoreProducts;
- (NSArray<SKProduct*>*)getProducts;
- (void)purchaseProduct:(SKProduct*)product;
- (void)registerObserver:(id)observer selector:(SEL)selector name:name;
- (void)unregisterObserver:(id)observer name:name;
@end

#define StoreEventTransactionResult @"transactionResult"
#define StoreEventFetchProduct @"fetchResult"

